using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Zappar;

public class UserInterfaceController : MonoBehaviour
{
    public Button Button1;
    public Button Button2;

    public ZapparCamera TheARCamera;

    private void Awake()
    {
        Button1.onClick.AddListener(()=> {
            // turn on vuforia
            TheARCamera.enabled = !TheARCamera.enabled;
        });

        Button2.onClick.AddListener(() => {
            // turn on vuforia
            print("tap on button 2!");
        });
    }
}
